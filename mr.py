import gitlab
import os
origin = "https://gitlab.com/"
# Private token is set as an env var
gl = gitlab.Gitlab(origin, "vzbtAfy9__fChUgmk4M4", api_version='4')
gl.auth()
import sys, os, urllib3, argparse, pdb
# Silence the irritating insecure warnings. I'm not insecure you're insecure!
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
project = sys.argv[1]
sourcebr = sys.argv[2]
targetbr = sys.argv[3]
# Add the location of python-gitlab to the path so we can import it
# repo_top = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
# python_gitlab_dir = os.path.join(repo_top, "external/python-gitlab")
# sys.path.append(python_gitlab_dir)
# def getArgs():
#     parser = argparse.ArgumentParser(description='Three examples of using the REST API. 1st: Summarize all open MRs, 2nd: Post note to existing MR, 3rd: Create new issue in project.',
#         formatter_class=argparse.ArgumentDefaultsHelpFormatter)
#     parser.add_argument( "authkey", type=str, help="Personal access token for authentication with GitLab. Create one at https://gitlab.com/profile/personal_access_tokens" )
#     parser.add_argument( "project", type=str,default="gitschooldude/hello",help="Path to GitLab project in the form <namespace>/<project>")
#     parser.add_argument( "--url",  help="Gitlab URL.", default='https://gitlab.com/')
#     return parser, parser.parse_args()
class PythonGitlabExample():
    def __init__(self, url, authkey, project,sourcebr,targetbr):
        # Parse command line arguments
        self.url = url
        self.authkey = authkey
        self.project_name = project
        self.sourcebr = sourcebr
        self.targetbr = targetbr
        self.project = None
        self.mrs = None  # list of all merge requests
        # Create python-gitlab server instance
        server = gitlab.Gitlab(self.url, self.authkey, api_version=4, ssl_verify=False)
        # Get an instance of the project and store it off
        self.project = server.projects.get(self.project_name)
    def list_mrs(self):
        # Get all MRs in the project and print some info about them
        try:
            mrs = self.project.mergerequests.list(all=True, state='opened')
            # print("All Merge Requests in project: %s" % self.project_name)
            if len(mrs):
                for mr in mrs:
                    print("  Merge Request ID: %d, Title: %s" % (mr.iid, mr.title))
                    print('    Labels: ' + ','.join(mr.labels))
                    closes = [str(c.iid) for c in mr.closes_issues()]
                    print('    Closes Issues: ' + ','.join(closes))
                    print('    Number of notes in discussion: ' + str(len(mr.notes.list())))
            else:
                print("no merge requests found")
        except:
                print("some error")
    def create_merge_request(self):
        title = ('merge request from {0} to {1}'.format(sourcebr,targetbr))
        try:
            mr = self.project.mergerequests.create({'source_branch': self.sourcebr,
                                    'target_branch': self.targetbr,
                                    'title': title,
                                    'assignee_id' : 8637272
                                    # 'labels': ['label1', 'label2']
                                    })
            mr.save()
        except Exception as e:
            print(e)
    def run(self):
        # if input("Example 1: Summarize all open merge requests.    Press enter to run, any other key to skip: ") == '': self.list_mrs()
        self.create_merge_request()
if __name__ == '__main__':
    # myParser, myargs = getArgs()
    if len(sys.argv) != 4:
        sys.exit("there are missing arguments")
    else:
        sys.exit(PythonGitlabExample(url="https://gitlab.com/", authkey="vzbtAfy9__fChUgmk4M4",
            # project="root/cloudifyops-test",sourcebr="feature_1",targetbr="develop").run())
            project=project,sourcebr=sourcebr,targetbr=targetbr).run())
